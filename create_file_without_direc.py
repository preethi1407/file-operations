# Python program to demonstrate
# creation of new file
import os
def create_file(file_name,extension):
    '''create_file(file_name,extension) function is used to create a new file with the given file name and extension at the path provided by the user

Arguments:
    file_name:String
    extension:String

Returns:
        return type:String
    '''
    file = file_name+extension
    # to return all the directories at the given path
    dir_list = os.listdir(path)
    # to check if a file already exists
    if file in dir_list:
        print("file exists")
    # to create a new file if the file does not exist
    else:
        with open(os.path.join(path, file), 'w') as fp:
            pass
            fp.write("New file created")
        # After creating the file returing the path
        return(path+"\\"+file)

#Driver program
# Specifying the path
path = r"C:\Users\pkommabathula\Desktop\file"
file_name = input("enter the file_name : ")
extension = input("enter the extension : ")
print(create_file(file_name,extension)) #function call  

   